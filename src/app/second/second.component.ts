import { Component, OnInit } from '@angular/core';
import { UserserviceService } from '../userservice.service';
@Component({
  selector: 'app-second',
  templateUrl: './second.component.html',
  styleUrls: ['./second.component.css']
})
export class SecondComponent implements OnInit {
  user:string;
    constructor(private userserviceService:UserserviceService) { }
  
    ngOnInit() {
      this.userserviceService.cast.subscribe(user=>this.user=user)
    }

}
