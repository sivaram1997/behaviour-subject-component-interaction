import { Component, OnInit } from '@angular/core';
import { UserserviceService } from '../userservice.service';
@Component({
  selector: 'app-first',
  templateUrl: './first.component.html',
  styleUrls: ['./first.component.css']
})
export class FirstComponent implements OnInit {
user1:string;
edit:string;
  constructor(private userserviceService:UserserviceService) { }

  ngOnInit() {
    this.userserviceService.cast.subscribe(user=>this.user1=user)
  }
  editValue(){
    // this.userserviceService.editData(this.edit)
    this.userserviceService.editData(this.edit)
  }

}
