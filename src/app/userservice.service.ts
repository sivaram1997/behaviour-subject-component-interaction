import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
@Injectable({
  providedIn: 'root'
})
export class UserserviceService {
private user = new  BehaviorSubject<string>('');
cast = this.user.asObservable();
  constructor() { }
  editData(users){
    this.user.next(users);
  }
}
